import unittest
import kadm5
from iam.realm import RealmManagementSession


class AccountTestCase(unittest.TestCase):

    def setUp(self):
        kadm5.Kadmin("dwhitla", "TEST.WOTIFGROUP.COM", password="P314159265i")
        self.session = RealmManagementSession("TEST.WOTIFGROUP.COM")
        self.default_person_group = self.session.get_string_config_parameter("person_group_default")

    def tearDown(self):
        if self.session:
            self.session.close()

    def test_group_resolution(self):
        next_gid_number = self.session.get_next_gid_number()
        self.session.create_group("dudes")
        self.assertEqual("dudes", self.session.group_with_gid_number(next_gid_number))
        self.session.delete_group("dudes")
        self.session.create_person_user("adude", groups=["dudes"])
        self.assertTrue(self.session.has_group("dudes"))
        self.assertEqual(self.default_person_group, self.session.get_primary_group("adude"))
        self.assertEqual(["dudes"], self.session.get_auxiliary_groups("adude"))
        self.session.delete_group("dudes")
        self.session.delete_user("adude")

    def test_groups(self):
        self.session.create_group("assholes")
        self.session.create_person_user("sasshole", password="14m4n455h0l3", cn="Some Asshole", groups="assholes")
        self.assertTrue(self.session.user_is_member_of_group("sasshole", "sg_users"))
        self.assertTrue(self.session.user_is_member_of_group("sasshole", "assholes"))
        self.session.create_person_user("aasshole", password="14m4n455h0l3", cn="Another Asshole", groups="assholes")
        self.assertTrue(self.session.user_is_member_of_group("aasshole", "sg_users"))
        self.assertTrue(self.session.user_is_member_of_group("aasshole", "assholes"))
        self.session.remove_user_from_group("aasshole", "assholes")
        self.assertFalse(self.session.user_is_member_of_group("aasshole", "assholes"))
        self.assertTrue(self.session.user_is_member_of_group("sasshole", "assholes"))
        self.session.delete_user("aasshole")
        self.session.delete_user("sasshole")
        self.assertFalse(self.session.user_is_member_of_group("sasshole", "assholes"))
        self.session.delete_group("assholes")
        self.assertFalse(self.session.user_is_member_of_group("sasshole", "assholes"))

    def test_something(self):
        uidn = self.session.get_uid_number("oracle")
        self.assertEqual(uidn, 1005, "oracle uidn is 1005")
        gidn = self.session.get_gid_number("sg_oracle")
        self.assertEqual(gidn, 10003, "sg_oracle gidn is 10003")
        self.session.create_person_user("sasshole", password="14m4n455h0l3", cn="Some Asshole")
        self.assertTrue(self.session.has_user("sasshole"))
        self.session.deactivate_user("sasshole")
        self.session.delete_user("sasshole")
        self.assertFalse(self.session.has_user("sasshole"))


if __name__ == '__main__':
    unittest.main()
