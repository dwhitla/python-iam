from ConfigParser import ConfigParser, NoSectionError
import os
import re
import socket
import sys

from dns.name import Name
import dns.resolver
from kadm5 import KadminError, KadminCallError, Kadmin
import ldap
import ldap.sasl

from ocsutil import ssh
from ocsutil.console import abort, info, debug, error, warn
from iam.ldap_search import ldap_search_results, del_tree


__all__ = ['RealmManagementError', 'RealmManagementSession']

_CONFIG_DEFAULTS = {
    "ldap_query_timeout": "5",
    "person_user_id_min": "10000",
    "daemon_user_id_min": "1000",
    "daemon_user_id_max": "9998",
    "person_group_default": "users",
    "person_home_base": "/home",
    "daemon_group_default": "daemon",
    "daemon_home_base": "/usr/share",
    "group_id_min": "1000",
    "group_id_max": "9999",
    "dns_root_rdn": "ou=dns",
    "account_name_length_limit": "20",
}


def glob_to_regex(text):
    for pattern, replacement in (
            (r"\.", r"\."),
            (r"\*", r".*"),
            (r"\?", r".")
    ):
        text = re.sub(pattern, replacement, text)
    return text


class RealmManagementError(Exception):
    def __init__(self, message, cause=None):
        super(RealmManagementError, self).__init__()
        self.message = str(message)
        self.cause = None
        if cause:
            detail = ""
            if hasattr(cause, "message"):
                if type(cause.message) == dict:
                    cause_message = cause.message.pop('desc', '')
                    detail = "\n".join(("%s: %s" % (k, v) for (k, v) in cause.message.iteritems()))
                else:
                    cause_message = repr(cause.message)
            else:
                cause_message = str(cause)
            self.cause = "Cause: %s%s" % (cause_message, detail)

    def __str__(self):
        return "%s\n%s" % (self.message, self.cause)


class MissingConfigurationKeyError(RealmManagementError):
    def __init__(self, section, key):
        super(MissingConfigurationKeyError, self).__init__(
            "Unable to look up the %s key for section [%s] in any config file" % (key, section),
            "There is probably no section [%s] in either ~/.config/python-iam/realm.conf or /etc/python-iam/realm.conf" %
            section)


class RealmManagementSession(object):
    """docstring for AccountSession"""

    MOD_REPLACE = ldap.MOD_REPLACE
    MOD_APPEND = ldap.MOD_ADD

    def __init__(self, realm=None, debug=False, config_file=None):
        super(RealmManagementSession, self).__init__()
        self.con = None
        self.kadmin = None
        self.debug = debug
        if realm:
            realm = realm.upper()
            domain = target = dns.name.from_text(realm.lower())
        else:
            domain = target = dns.name.from_text(socket.getfqdn()).parent()
        _kerberos = dns.name.from_text("_kerberos", dns.name.empty)
        while realm is None and domain != dns.name.root:
            qname = _kerberos + domain
            try:
                answers = dns.resolver.query(qname, dns.rdatatype.TXT)
                realm = answers[0].to_text().strip('"')
            except dns.resolver.NoNameservers:
                raise RealmManagementError("Unable to contact any nameservers for %s" % domain)
            except dns.resolver.NXDOMAIN:
                try:
                    domain = domain.parent()
                except dns.name.NoParent:
                    break
            except dns.exception.Timeout:
                raise RealmManagementError("Timeout resolving a _kerberos TXT record for %s or one of its superdomains"
                                           % target.to_text(omit_final_dot=True))
            except dns.exception.DNSException, e:
                raise RealmManagementError("Error resolving the _kerberos record for the %s domain" % domain, e)

        if realm is None:
            raise RealmManagementError(
                "No realm was specified and a _kerberos TXT record could not be found for %s or any of its superdomains"
                % target.to_text(omit_final_dot=True))
        self.realm = realm

        cp = ConfigParser(_CONFIG_DEFAULTS)
        cp.read(config_file if config_file else ["/etc/python-iam/realm.conf",
                                                 os.path.expanduser("~/.config/python-iam/realm.conf")])
        self.config = cp

        try:
            self.kadmin = Kadmin(realm_name=self.realm)
        except KadminError, e:
            raise RealmManagementError("Error obtaining an admin ticket for the '%s' realm" % self.realm, e)

        self.domain = domain
        # for now just derive ldap_base from the domain name
        self.ldap_base = "dc=" + ",dc=".join(domain.labels[:-1])
        self._bind("sasl", debug=self.debug)

    def __enter__(self):
        return self

    #noinspection PyUnusedLocal
    def __exit__(self, type_, value, traceback):
        self.close()

    def __del__(self):
        self.close()
        sup = super(RealmManagementSession, self)
        if hasattr(sup, '__del__'):
            sup.__del__()

    def _bind(self, bind_dn="", bind_pw="", debug=False, async=False, log=sys.stderr):
        # look up the ldap services in dns
        ldap_servers = []
        _ldap = dns.name.from_text("_ldap._tcp", dns.name.empty)
        qname = _ldap + self.domain
        try:
            answers = dns.resolver.query(qname, dns.rdatatype.SRV)
            for srv_record in answers:
                hostname = srv_record.target.to_text(omit_final_dot=True)
                for txt_record in dns.resolver.query(hostname, dns.rdatatype.TXT):
                    if txt_record.to_text() == '"ldaptype: master"':
                        ldap_servers.append((srv_record.priority, hostname, srv_record.port))
                        break
        except dns.resolver.NXDOMAIN:
            raise RealmManagementError("Unable to discover any LDAP services for the %s domain" % self.domain)
        except dns.exception.DNSException, e:
            raise RealmManagementError("Unable to resolve LDAP services for the %s domain" % self.domain, e)

        # order ldap servers by descending priority
        ldap_servers.sort(key=lambda tup: tup[0])
        for ldap_server in ldap_servers:
            url = "ldap://%s:%i" % (ldap_server[1], ldap_server[2])
            con = ldap.initialize(url, (1 if debug else 0), log)
            con.set_option(ldap.OPT_X_TLS_DEMAND, True)
            con.set_option(ldap.OPT_TIMEOUT, self.get_float_config_parameter("ldap_query_timeout"))
            try:
                con.start_tls_s()
                if bind_dn == "sasl":
                    if not ldap.SASL_AVAIL:
                        raise RealmManagementError("SASL binds are not available from this host.")
                    con.sasl_interactive_bind_s("", ldap.sasl.gssapi(), sasl_flags=(0 if debug else ldap.SASL_QUIET))
                else:
                    if async:
                        m_id = con.simple_bind(bind_dn, bind_pw)
                        con.result(m_id)
                    else:
                        con.simple_bind_s(bind_dn, bind_pw)
                self.con = con
                return
            except ldap.INVALID_CREDENTIALS:
                raise RealmManagementError("Your supplied credentials were incorrect.")
            except ldap.SERVER_DOWN:
                pass
            except ldap.CONNECT_ERROR:
                raise RealmManagementError(
                    "Unable to establish a secure (TLS) connection to %s." % url,
                    "This is most likely due to the server certificate not being trusted on this machine. "
                    "Check your SSL CA certificates.")
            except ldap.LDAPError, e:
                raise RealmManagementError(
                    "An unknown error occurred while attempting to bind to %s. "
                    "Try enabling debugging (-vv) for more information as to the possible cause." % url, e)
        raise RealmManagementError("Unable to contact any writeable LDAP server for %s" % self.domain)

    def rebind_as(self, bind_dn="", bind_pw="", debug=False, async=False, log=sys.stderr):
        self._unbind()
        self._bind(bind_dn, bind_pw, debug, async, log)

    def _unbind(self):
        if self.con:
            try:
                self.con.unbind()
            except ldap.LDAPError:
                pass
            finally:
                self.con = None

    def close(self):
        if self.kadmin:
            self.kadmin.close()
        self._unbind()

    def _abort_with_error(self, err, cleanuphook=None):
        detail = None
        if type(err) == str:
            message = err
        elif type(err.message) == dict:
            message = err.message.pop('desc', '')
            detail = "\n".join(("%s: %s" % (k, v) for (k, v) in err.message.iteritems()))
        else:
            message = err.message
        if cleanuphook is None:
            cleanuphook = self.close
        abort("Error: %s." % message, detail, cleanuphook)

    def get_string_config_parameter(self, key):
        try:
            return self.config.get(self.realm, key)
        except NoSectionError:
            raise MissingConfigurationKeyError(self.realm, key)

    def get_int_config_parameter(self, key):
        try:
            return self.config.getint(self.realm, key)
        except NoSectionError:
            raise MissingConfigurationKeyError(self.realm, key)

    def get_boolean_config_parameter(self, key):
        try:
            return self.config.getboolean(self.realm, key)
        except NoSectionError:
            raise MissingConfigurationKeyError(self.realm, key)

    def get_float_config_parameter(self, key):
        try:
            return self.config.getfloat(self.realm, key)
        except NoSectionError:
            raise MissingConfigurationKeyError(self.realm, key)

    # USERS

    def get_uid_number(self, uid):
        try:
            raw_results = self.con.search_s(
                "uid=%s,ou=users,%s" % (uid, self.ldap_base),
                ldap.SCOPE_BASE,
                filterstr="(objectClass=posixAccount)",
                attrlist=["uidNumber"]
            )
            return int(ldap_search_results(raw_results)[0].get_attr_values("uidNumber")[0])
        except ldap.LDAPError, e:
            raise RealmManagementError("", e)

    def has_user(self, uid):
        result = None
        try:
            self.con.search_s(
                "uid=%s,ou=users,%s" % (uid, self.ldap_base),
                ldap.SCOPE_BASE,
                filterstr="(objectClass=posixAccount)",
                attrlist=["dn"]
            )
            debug("User '%s' exists" % uid)
            result = True
        except ldap.NO_SUCH_OBJECT:
            debug("User '%s' does not exist" % uid)
            result = False
        except ldap.LDAPError, e:
            raise RealmManagementError("", e)
        finally:
            return result

    def get_next_person_uid_number(self):
        try:
            raw_results = self.con.search_s(
                "ou=users,%s" % self.ldap_base,
                ldap.SCOPE_ONELEVEL,
                filterstr="(objectClass=person)",
                attrlist=["uidNumber"]
            )
            results = ldap_search_results(raw_results)
            uidnumbers = []
            for result in results:
                uidnumbers.append(int(result.get_attr_values("uidNumber")[0]))
            minuidnumber = self.get_int_config_parameter("person_user_id_min")
            return max(minuidnumber, max(uidnumbers) + 1) if uidnumbers else minuidnumber
        except ldap.LDAPError, e:
            raise RealmManagementError("Unable to calculate the next uidNumber.", e)

    def get_next_service_uid_number(self):
        try:
            raw_results = self.con.search_s(
                "ou=users,%s" % self.ldap_base,
                ldap.SCOPE_ONELEVEL,
                filterstr="(objectClass=account)",
                attrlist=["uidNumber"]
            )
            uidnumbers = []
            maxuidnumber = self.get_int_config_parameter("daemon_user_id_max")
            for result in ldap_search_results(raw_results):
                uidnumber = int(result.get_attr_values("uidNumber")[0])
                if uidnumber < maxuidnumber:
                    uidnumbers.append(uidnumber)
            minuidnumber = self.get_int_config_parameter("daemon_user_id_min")
            return max(minuidnumber, max(uidnumbers) + 1) if uidnumbers else minuidnumber
        except ldap.LDAPError, e:
            raise RealmManagementError("Unable to calculate the next uidNumber.", e)

    def create_person_user(self, uid, password=None, cn=None, groups=None, homedir=None, quota=None):
        if cn is None:
            cn = uid
        names = re.split(r" +", cn, 1)
        if len(names) == 1:
            names[0] = str(cn[:1])
            names.append(str(cn[1:]))
        names[0] = names[0].title()
        names[1] = names[1].title()

        if groups is None:
            groups = []
        elif not type(groups) == list:
            groups = [groups]
        primarygroup = self.get_string_config_parameter("person_group_default")
        if primarygroup in groups:
            groups.remove(primarygroup)
        if homedir is None:
            homedir = ("%s/%s" % (self.get_string_config_parameter("person_home_base"), uid))
        accountstatus = "inactive" if password is None else "active"
        quota_slimit = quota_hlimit = quota_sinodes = quota_hinodes = 0
        if not quota is None:
            _quota = int(quota)
            quota_hlimit = _quota * 256
            quota_slimit = int(quota_hlimit * 0.99)
            quota_sinodes = 0
            quota_hinodes = 0

        if self.has_user(uid):
            raise RealmManagementError("User '%s' already exists" % uid)

        dn = "uid=%s,ou=users,%s" % (uid, self.ldap_base)
        domain = self.domain.to_text(omit_final_dot=True)
        try:
            info("Creating user '%s' in the '%s' realm" % (uid, self.realm))
            self.con.add_s(dn, [
                ("objectClass", [
                    "inetOrgPerson", "krbPrincipalAux", "organizationalPerson",
                    "person", "posixAccount", "systemQuotas", "top", "wotifGroupUser"
                ]),
                ("accountStatus", accountstatus),
                ("cn", cn),
                ("displayName", cn),
                ("gidNumber", str(self.get_gid_number(primarygroup))),
                ("givenName", names[0]),
                ("homeDirectory", homedir),
                ("jabber", "%s@%s" % (uid, domain)),
                ("krbCanonicalName", "%s@%s" % (uid, self.realm)),
                ("loginShell", "/bin/bash"),
                ("mail", "%s@%s" % (str.lower(cn).replace(' ', '.'), domain)),
                ("quota", "/home,%i,%i,%i,%i" % (quota_hlimit, quota_slimit, quota_sinodes, quota_hinodes)),
                ("sn", names[1]),
                ("uidNumber", str(self.get_next_person_uid_number())),
                ("userPassword", "{SASL}%s@%s" % (uid, self.realm))
            ])
            self.kadmin.create_principal(uid, password, policy="", attributes=["+needchange", "+requires_preauth"],
                                         db_args='dn="%s"' % dn)
            for group in groups:
                if not self.has_group(group):
                    self.create_group(group)
                self.add_user_to_group(uid, group)

            # create the user's home directory
            mkhomedir = ssh.execute("root@nfs.test.wotifgroup.com",
                                    "cp -r /etc/skel %s && chown -R %s:%s %s" % (homedir, uid, primarygroup, homedir))
            if mkhomedir.failed:
                warn("Could not create the user's home directory", mkhomedir.stderr)

        except ldap.LDAPError, e:
            raise RealmManagementError("Could not create user '%s' - LDAP add failed" % uid, e)
        except KadminError, e:
            self.con.delete_s(dn)
            raise RealmManagementError("Could not create user '%s' - Kerberos create_principal failed" % uid, e)
        except KadminCallError, e:
            self.con.delete_s(dn)
            raise e

    def create_daemon_user(self, uid, password=None, cn=None, primary_group=None, groups=None, homedir=None,
                           quota=None):
        account_name_length_limit = self.get_int_config_parameter("account_name_length_limit")
        if len(uid) > account_name_length_limit:
            raise RealmManagementError("User names must not be longer than %i characters." % account_name_length_limit)
        if cn is None:
            cn = uid
        if groups is None:
            groups = []
        elif not type(groups) == list:
            groups = [groups]
        default_group = self.get_string_config_parameter("daemon_group_default")
        if primary_group is None:
            primary_group = default_group
        if primary_group in groups:
            groups = groups.remove(primary_group)
        if not primary_group == default_group and not self.has_group(primary_group):
            self.create_group(primary_group)
        if homedir is None:
            homedir = ("%s/%s" % (self.get_string_config_parameter("daemon_home_base"), uid))
        quota_slimit = quota_hlimit = quota_sinodes = quota_hinodes = 0
        if not quota is None:
            _quota = int(quota)
            quota_hlimit = _quota * 256
            quota_slimit = int(quota_hlimit * 0.99)
            quota_sinodes = 0
            quota_hinodes = 0

        if self.has_user(uid):
            raise RealmManagementError("User '%s' already exists" % uid)

        dn = "uid=%s,ou=users,%s" % (uid, self.ldap_base)
        try:
            info("Creating user '%s' in the '%s' realm" % (uid, self.realm))
            self.con.add_s(dn, [
                ("objectClass", ["posixAccount", "systemQuotas", "top", "account"]),
                ("cn", cn),
                ("gidNumber", str(self.get_gid_number(primary_group))),
                ("homeDirectory", homedir),
                ("loginShell", "/sbin/nologin"),
                ("quota", "/,%i,%i,%i,%i" % (quota_hlimit, quota_slimit, quota_sinodes, quota_hinodes)),
                ("uidNumber", str(self.get_next_service_uid_number())),
                ("userPassword", "{CRYPT}*")
            ])
            self.kadmin.create_principal(uid, password, attributes="+allow_svr", db_args='dn="%s"' % dn)
            for group in groups:
                if not self.has_group(group):
                    self.create_group(group)
                self.add_user_to_group(uid, group)
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not create user '%s' - LDAP add failed" % uid, e)
        except KadminError, e:
            self.con.delete_s(dn)
            raise RealmManagementError("Could not create user '%s' - Kerberos create_principal failed" % uid, e)
        except KadminCallError, e:
            self.con.delete_s(dn)
            raise e

    def set_primary_group(self, user, group):
        try:
            self.modify_user(user, [(self.MOD_REPLACE, "gidNumber", str(self.get_gid_number(group)))])
        except Exception, e:
            raise RealmManagementError("Unable to set primary group for '%s'" % user, e)

    def set_user_common_name(self, user, cn):
        try:
            self.modify_user(user, [(self.MOD_REPLACE, "cn", cn)])
        except Exception, e:
            raise RealmManagementError("Unable to set common name for '%s'" % user, e)

    def set_user_home(self, user, homedir):
        try:
            self.modify_user(user, [(self.MOD_REPLACE, "homeDirectory", homedir)])
        except Exception, e:
            raise RealmManagementError("Unable to set home directory for '%s'" % user, e)

    def set_user_account_expiry(self, user, expiredate):
        try:
            self.modify_user(user, [(self.MOD_REPLACE, "krb5Expires", expiredate)])
        except Exception, e:
            raise RealmManagementError("Unable to set account expiry for '%s'" % user, e)

    def modify_user(self, user, updates):
        for update_action, attribute, value in updates:
            if update_action not in [self.MOD_REPLACE, self.MOD_APPEND]:
                raise RealmManagementError(
                    "Unable to modify attribute '%s' for user '%s'" % (attribute, user),
                    "'%s' is not a valid action" % str(update_action))
        try:
            self.con.modify_s("uid=%s,ou=users,%s" % (user, self.ldap_base), updates)
        except ldap.NO_SUCH_OBJECT:
            raise RealmManagementError("User '%s' does not exist" % user)
        except ldap.LDAPError, e:
            raise RealmManagementError("ldap_modify operation failed:\n%s" % repr(e))

    def rename_user(self, user, new_name):
        account_name_length_limit = self.get_int_config_parameter("account_name_length_limit")
        if len(new_name) > account_name_length_limit:
            raise RealmManagementError("User names must not be longer than %i characters." % account_name_length_limit)
        try:
            self.con.rename_s("uid=%s,ou=users,%s" % (user, self.ldap_base), "uid=%s" % new_name)
        except Exception, e:
            raise RealmManagementError("Unable to rename user '%s'" % user, e)

    def set_user_shell(self, user, shell):
        try:
            self.modify_user(user, [(self.MOD_REPLACE, "loginShell", shell)])
        except Exception, e:
            raise RealmManagementError("Unable to set login shell for '%s'" % user, e)

    def delete_user(self, uid, delete_homedir=False):
        if not self.has_user(uid):
            raise RealmManagementError("User '%s' does not exist" % uid)

        for group in self.get_auxiliary_groups(uid):
            self.remove_user_from_group(uid, group)
        primarygroup = self.get_primary_group(uid)
        if delete_homedir:
            homedir = self.get_home_dir(uid)
            if homedir.startswith(self.get_string_config_parameter("person_home_base")):
                info("Deleting '%s's home directory" % uid)
                delhomedir = ssh.execute("root@nfs.test.wotifgroup.com", "rm -rf %s" % homedir)
                if delhomedir.failed:
                    warn("Could not delete the user's home directory", delhomedir.stderr)
        try:
            info("Deleting user '%s' in the '%s' realm" % (uid, self.realm))
            self.con.delete_s("uid=%s,ou=users,%s" % (uid, self.ldap_base))
        except ldap.LDAPError:
            raise RealmManagementError("Could not delete user '%s' - LDAP delete failed" % uid)
        if not primarygroup in (
                self.get_string_config_parameter("person_group_default"),
                self.get_string_config_parameter("daemon_group_default")
        ) and not self.get_members(primarygroup):
            self.delete_group(primarygroup)

    def reset_user_password(self, user, password):
        try:
            self.kadmin.change_password(user, password)
            self.kadmin.modify_principal(user, attributes="+needchange")
        except KadminError, e:
            raise RealmManagementError("Could not reset password for user '%s'." % user, e)

    def deactivate_user(self, uid):
        if not self.has_user(uid):
            raise RealmManagementError("User '%s' does not exist" % uid)

        dn = "uid=%s,ou=users,%s" % (uid, self.ldap_base)
        try:
            debug("Deactivating user '%s' in the '%s' realm" % (uid, self.realm))
            self.con.modify_s(dn, [(ldap.MOD_REPLACE, "accountStatus", "inactive")])
            self.kadmin.modify_principal(uid, expires="now")
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not deactivate user '%s' - LDAP modify failed" % uid, e)
        except KadminError, e:
            raise RealmManagementError("Could not deactivate user '%s' - Kerberos modify_principal failed" % uid, e)

    def activate_user(self, uid):
        if not self.has_user(uid):
            raise RealmManagementError("User '%s' does not exist" % uid)

        dn = "uid=%s,ou=users,%s" % (uid, self.ldap_base)
        try:
            info("Activating user '%s' in the '%s' realm" % (uid, self.realm))
            self.con.modify_s(dn, [(ldap.MOD_REPLACE, "accountStatus", "active")])
            self.kadmin.modify_principal(uid, expires="never")
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not activate user '%s' - LDAP modify failed" % uid, e)
        except KadminError, e:
            raise RealmManagementError("Could not activate user '%s' - Kerberos modify_principal failed" % uid, e)

    def get_home_dir(self, uid):
        try:
            raw_results = self.con.search_s(
                "uid=%s,ou=users,%s" % (uid, self.ldap_base),
                ldap.SCOPE_BASE,
                attrlist=["homeDirectory"]
            )
            return ldap_search_results(raw_results)[0].get_attr_values("homeDirectory")[0]
        except ldap.NO_SUCH_OBJECT:
            raise RealmManagementError("User '%s' does not exist" % uid)
        except ldap.LDAPError, e:
            raise RealmManagementError("Generic LDAP error retrieving the home directory for user '%s'" % uid, e)

    # GROUPS

    def get_gid_number(self, gid):
        try:
            raw_results = self.con.search_s(
                "cn=%s,ou=groups,%s" % (gid, self.ldap_base),
                ldap.SCOPE_BASE,
                filterstr="(objectClass=posixGroup)",
                attrlist=["gidNumber"]
            )
            return int(ldap_search_results(raw_results)[0].get_attr_values("gidNumber")[0])
        except ldap.NO_SUCH_OBJECT:
            raise RealmManagementError("Group '%s' does not exist" % gid)
        except ldap.LDAPError, e:
            raise RealmManagementError("", e)

    def has_group(self, gid):
        result = None
        try:
            self.con.search_s(
                "cn=%s,ou=groups,%s" % (gid, self.ldap_base),
                ldap.SCOPE_BASE,
                filterstr="(objectClass=posixGroup)",
                attrlist=["dn"]
            )
            debug("Group '%s' exists" % gid)
            result = True
        except ldap.NO_SUCH_OBJECT:
            debug("Group '%s' does not exist" % gid)
            result = False
        except ldap.LDAPError, e:
            raise RealmManagementError("", e)
        finally:
            return result

    def get_next_gid_number(self):
        try:
            raw_results = self.con.search_s(
                "ou=groups,%s" % self.ldap_base,
                ldap.SCOPE_ONELEVEL,
                filterstr="(objectClass=posixGroup)",
                attrlist=["gidNumber"]
            )
            gidnumbers = []
            for result in ldap_search_results(raw_results):
                gidnumber = int(result.get_attr_values("gidNumber")[0])
                if gidnumber < self.get_int_config_parameter("group_id_max"):
                    gidnumbers.append(gidnumber)
            mingidnumber = self.get_int_config_parameter("group_id_min")
            return max(mingidnumber, max(gidnumbers) + 1) if gidnumbers else mingidnumber
        except ldap.LDAPError, e:
            raise RealmManagementError("", e)

    def create_group(self, group):
        account_name_length_limit = self.get_int_config_parameter("account_name_length_limit")
        if len(group) > account_name_length_limit:
            raise RealmManagementError("Group names must not be longer than %i characters." % account_name_length_limit)
        try:
            info("Creating the %s group" % group)
            self.con.add_s("cn=%s,ou=groups,%s" % (group, self.ldap_base), [
                ("objectClass", ["posixGroup", "top"]),
                ("gidNumber", str(self.get_next_gid_number()))
            ])
        except ldap.LDAPError, e:
            error("Could not create the '%s' group" % group)
            raise RealmManagementError("", e)

    def is_primary_group(self, group):
        gidnumber = self.get_gid_number(group)
        uids = []
        try:
            raw_results = self.con.search_s(
                "ou=users,%s" % self.ldap_base,
                ldap.SCOPE_ONELEVEL,
                filterstr="(&(objectClass=posixAccount)(gidNumber=%i))" % gidnumber,
                attrlist=["uid"]
            )
            for user_match in ldap_search_results(raw_results):
                uids.append(user_match.get_attr_values("uid")[0])
        except ldap.NO_SUCH_OBJECT:
            pass
        return uids

    def group_with_gid_number(self, gidnumber):
        try:
            raw_results = self.con.search_s(
                "ou=groups,%s" % self.ldap_base,
                ldap.SCOPE_ONELEVEL,
                filterstr="(&(objectClass=posixGroup)(gidNumber=%i))" % gidnumber,
                attrlist=["cn"]
            )
            return ldap_search_results(raw_results)[0].get_attr_values("cn")[0]
        except ldap.NO_SUCH_OBJECT:
            info("There is no group with gidNumber '%i' in the '%s' realm" % (gidnumber, self.realm))
            return None

    def get_primary_group(self, user):
        try:
            raw_results = self.con.search_s(
                "uid=%s,ou=users,%s" % (user, self.ldap_base),
                ldap.SCOPE_BASE,
                filterstr="(objectClass=posixAccount)",
                attrlist=["gidNumber"]
            )
            gidnumber = int(ldap_search_results(raw_results)[0].get_attr_values("gidNumber")[0])
            return self.group_with_gid_number(gidnumber)
        except ldap.NO_SUCH_OBJECT:
            raise RealmManagementError("User '%s' does not exist" % user)

    def get_auxiliary_groups(self, user):
        groups = []
        try:
            raw_results = self.con.search_s(
                "ou=groups,%s" % self.ldap_base,
                ldap.SCOPE_ONELEVEL,
                filterstr="(&(objectClass=posixGroup)(memberUid=%s))" % user,
                attrlist=["cn"]
            )
            for group_match in ldap_search_results(raw_results):
                groups.append(group_match.get_attr_values("cn")[0])
        except ldap.NO_SUCH_OBJECT:
            info("User '%s' is not a member of any auxiliary groups" % user)
        return groups

    def get_members(self, group):
        try:
            raw_results = self.con.search_s(
                "cn=%s,ou=groups,%s" % (group, self.ldap_base),
                ldap.SCOPE_BASE,
                filterstr="(objectClass=posixGroup)",
                attrlist=["memberUid"]
            )
            members = ldap_search_results(raw_results)[0].get_attr_values("memberUid")
            return members if members else []
        except ldap.NO_SUCH_OBJECT:
            raise RealmManagementError("Group '%s' does not exist." % group)
        except ldap.LDAPError, e:
            raise RealmManagementError("Unexpected LDAP error while getting members of group '%s'" % group, e)

    def delete_group(self, group):
        if not self.has_group(group):
            raise RealmManagementError("Group '%s' does not exist." % group)

        if self.is_primary_group(group):
            raise RealmManagementError("You cannot delete a user's primary group '%s'." % group)
        dn = "cn=%s,ou=groups,%s" % (group, self.ldap_base)
        try:
            info("Deleting group '%s' in the '%s' realm" % (group, self.realm))
            self.con.delete_s(dn)
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not delete group '%s' - LDAP delete failed." % group, e)

    def user_is_member_of_group(self, user, group):
        try:
            raw_results = self.con.search_s(
                "uid=%s,ou=users,%s" % (user, self.ldap_base),
                ldap.SCOPE_BASE,
                filterstr="(objectClass=posixAccount)",
                attrlist=["gidNumber"]
            )
            debug("User '%s' exists" % user)
            if self.get_gid_number(group) == int(ldap_search_results(raw_results)[0].get_attr_values("gidNumber")[0]):
                return True
        except ldap.NO_SUCH_OBJECT:
            debug("User '%s' does not exist" % user)
            return False
        return user in self.get_members(group)

    def add_user_to_group(self, user, group):
        if not self.has_user(user):
            raise RealmManagementError("Could not add user '%s' to the '%s' group." % (user, group),
                                       "There is no user named %s." % user)
        if self.user_is_member_of_group(user, group):
            info("%s is already a member of the %s group" % (user, group))
            return
        try:
            info("Adding user '%s' to the '%s' group" % (user, group))
            if not self.has_group(group):
                raise RealmManagementError("Could not add user '%s' to the nonexistent '%s' group" % (user, group))
            self.con.modify_s("cn=%s,ou=groups,%s" % (group, self.ldap_base), [(ldap.MOD_ADD, "memberUid", user)])
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not add user '%s' to the '%s' group." % (user, group), e)

    def remove_user_from_group(self, uid, group):
        if not self.has_user(uid):
            raise RealmManagementError("Could not remove user '%s' from the '%s' group." % (uid, group),
                                       "There is no user named %s." % uid)
        if not self.user_is_member_of_group(uid, group):
            info("%s is not a member of the %s group" % (uid, group))
            return
        try:
            info("Removing user '%s' from the '%s' group" % (uid, group))
            self.con.modify_s("cn=%s,ou=groups,%s" % (group, self.ldap_base), [(ldap.MOD_DELETE, "memberUid", uid)])
        except ldap.NO_SUCH_OBJECT:
            info("Group '%s' does not exist" % group)
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not remove user '%s' from the '%s' group." % (uid, group), e)

    def rename_group(self, group, new_name):
        account_name_length_limit = self.get_int_config_parameter("account_name_length_limit")
        if len(new_name) > account_name_length_limit:
            raise RealmManagementError("Group names must not be longer than %i characters." % account_name_length_limit)
        try:
            self.con.rename_s("cn=%s,ou=groups,%s" % (group, self.ldap_base), "cn=%s" % new_name)
        except Exception, e:
            raise RealmManagementError("Unable to rename group '%s'" % group, e)

    # SERVICES

    def has_service(self, service):
        result = None
        try:
            self.con.search_s(
                "krbPrincipalName=%s,ou=services,%s" % (service, self.ldap_base),
                ldap.SCOPE_BASE,
                filterstr="(objectClass=krbPrincipal)",
                attrlist=["dn"]
            )
            debug("Service '%s' exists" % service)
            result = True
        except ldap.NO_SUCH_OBJECT:
            debug("Service '%s' does not exist" % service)
            result = False
        except ldap.LDAPError, e:
            raise RealmManagementError("", e)
        finally:
            return result

    def create_service_principal(self, principal):
        if self.has_service(principal):
            raise RealmManagementError("Service '%s' already exists" % principal)
        dn = "krbPrincipalName=%s,ou=services,%s" % (principal, self.ldap_base)
        try:
            info("Creating service '%s' in the '%s' realm" % (principal, self.realm))
            self.con.add_s(dn, [
                ("objectClass", ["krbPrincipal", "top"]),
                ("krbPrincipalName", principal)
            ])
            self.kadmin.create_principal(principal, policy="", attributes="+allow_svr", db_args='dn="%s"' % dn)
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not create service '%s' - LDAP add failed" % principal, e)
        except KadminError, e:
            self.con.delete_s(dn)
            raise RealmManagementError("Could not create service '%s' - Kerberos create_principal failed" %
                                       principal, e)

    # HOSTS

    def has_host(self, name):
        realm_relative_name, _, _ = self.relativize(name)
        host = realm_relative_name.to_text()
        fqdn = name.to_text(omit_final_dot=True) if isinstance(name, Name) else name
        try:
            self.con.search_s(
                "host=%s,ou=hosts,%s" % (host, self.ldap_base),
                ldap.SCOPE_BASE,
                attrlist=["dn"])
            debug("Host account exists for '%s'" % fqdn)
            return True
        except ldap.NO_SUCH_OBJECT:
            debug("Host account does not exist for '%s'" % fqdn)
        except ldap.LDAPError, e:
            raise RealmManagementError("", e)
        return False

    def create_host(self, name):
        fqdn = name.to_text(omit_final_dot=True) if isinstance(name, Name) else name
        realm_relative_name, _, _ = self.relativize(name)
        if self.has_host(name):
            raise RealmManagementError("Host '%s' already exists" % fqdn)
        host = realm_relative_name.to_text(omit_final_dot=True)
        info("Creating host account for '%s' in the '%s' realm" % (fqdn, self.realm))
        dn = "host=%s,ou=hosts,%s" % (host, self.ldap_base)
        principal = "host/%s" % fqdn
        try:
            self.con.add_s(dn, [
                ("objectClass", ["account", "top", "krbPrincipalAux"]),
                ("uid", host),
                ("krbCanonicalName", str("%s@%s" % (principal, self.realm))),
            ])
            self.kadmin.create_principal(
                principal,
                policy="host",
                attributes=["+requires_preauth"],
                db_args='dn="%s"' % dn
            )
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not create host account for '%s' - LDAP add failed." % fqdn, e)
        except KadminError, e:
            self.con.delete_s(dn)
            raise RealmManagementError(
                "Could not create host account for '%s' - Kerberos create_principal failed." % principal, e)
        try:
            self.con.modify_s(dn, [(ldap.MOD_ADD, "krbPrincipalAliases", "host/%s@%s" % (host, self.realm))])
        except ldap.LDAPError, e:
            self.con.delete_s(dn)
            raise RealmManagementError("Could not create host account for '%s' - LDAP modify failed" % fqdn, e)
        except KadminCallError, e:
            self.con.delete_s(dn)
            raise e

    def get_forward_zones(self):
        return self.get_hosted_zones_matching("forward")

    def get_reverse_zones(self):
        return self.get_hosted_zones_matching("reverse")

    def get_stub_zones(self):
        return self.get_hosted_zones_matching("stub")

    def get_hosted_zones_matching(self, types):
        if not type(types) == list:
            types = [types]
        zones = []
        try:
            for zone_type in types:
                if zone_type in ["forward", "reverse", "stub"]:
                    raw_results = self.con.search_s(
                        "cn=%s-zones,%s,%s" % (
                            zone_type,
                            self.get_string_config_parameter("dns_root_rdn"),
                            self.ldap_base
                        ),
                        ldap.SCOPE_ONELEVEL,
                        filterstr="(objectClass=dlzZone)",
                        attrlist=["dlzZoneName"]
                    )
                    for zone_record in ldap_search_results(raw_results):
                        zones.append(dns.name.from_text(zone_record.get_attr_values("dlzZoneName")[0]))
            zones.sort()
            return zones
        except ldap.LDAPError, e:
            raise RealmManagementError("Unable to enumerate hosted zones of type '%s'." % ' or '.join(types), e)

    def create_stub_zone(self, name):
        #todo
        pass

    def create_forward_zone(self, name):
        #todo
        pass

    def create_reverse_zone(self, name):
        #todo
        pass

    def copy_zone_mapping(self, tuple_):
        #todo
        pass

    def create_dns_a_record(self, name, ip):
        _name = dns.name.from_text(str(name))
        address = ip.strNormal()
        if ip.len() > 1:
            raise RealmManagementError("The A record IP '%s' is not a single IP address" % address)
        forward_zones = self.get_forward_zones()
        stub_zones = self.get_stub_zones()
        zone = _name.parent()
        zone_type = None
        try:
            while zone_type is None:
                if zone in forward_zones:
                    zone_type = "forward"
                elif zone in stub_zones:
                    zone_type = "stub"
                else:
                    zone = zone.parent()
        except dns.name.NoParent:
            raise RealmManagementError("The realm is not authoritative for %s." % _name.to_text(omit_final_dot=True))

        hostname = _name.relativize(zone)
        record = "%s IN A %s to %s" % (hostname, address, zone)
        info("Adding DNS record '%s'" % record)
        dlzhost_dn = "dlzHostName=%s,dlzZoneName=%s,cn=%s-zones,%s,%s" % (
            hostname,
            zone.to_text(omit_final_dot=True),
            zone_type,
            self.get_string_config_parameter("dns_root_rdn"),
            self.ldap_base
        )
        try:
            info("Creating DNS record '%s' in the '%s' realm" % (record, self.realm))
            self.con.add_s(dlzhost_dn, [("objectClass", "dlzHost")])
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not create DNS record '%s' - LDAP add failed" % record, e)
        try:
            self.con.add_s("dlzType=a,%s" % dlzhost_dn, [
                ("objectClass", "dlzARecord"),
                ("dlzHostName", hostname.to_text()),
                ("dlzIPAddr", address),
                ("dlzRecordID", "0"),
                ("dlzTTL", "3600"),
            ])
        except ldap.LDAPError, e:
            self.con.delete_s(dlzhost_dn)
            raise RealmManagementError("Could not create record '%s' - LDAP add failed" % record, e)

    def create_dns_cname_record(self, alias, target):
        forward_zones = self.get_forward_zones()
        stub_zones = self.get_stub_zones()
        _alias = dns.name.from_text(str(alias))
        zone = alias.parent()
        zone_type = None
        try:
            while zone_type is None:
                if zone in forward_zones:
                    zone_type = "forward"
                elif zone in stub_zones:
                    zone_type = "stub"
                else:
                    zone = zone.parent()
        except dns.name.NoParent:
            raise RealmManagementError("The realm is not authoritative for %s." % _alias.to_text(omit_final_dot=True))

        _alias = _alias.relativize(zone)
        target = target.relativize(zone)
        record = "%s IN CNAME %s to %s" % (_alias, target, zone)
        info("Adding DNS record '%s'" % record)
        dlzhost_dn = "dlzHostName=%s,dlzZoneName=%s,cn=%s-zones,%s,%s" % (
            _alias,
            zone.to_text(omit_final_dot=True),
            zone_type,
            self.get_string_config_parameter("dns_root_rdn"),
            self.ldap_base
        )
        try:
            info("Creating DNS record '%s' in the '%s' realm" % (record, self.realm))
            self.con.add_s(dlzhost_dn, [("objectClass", "dlzHost")])
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not create DNS record '%s' - LDAP add failed" % record, e)
        try:
            self.con.add_s("dlzType=cname,%s" % dlzhost_dn, [
                ("objectClass", "dlzCNAMERecord"),
                ("dlzHostName", _alias.to_text()),
                ("dlzData", target.to_text()),
                ("dlzRecordID", "0"),
                ("dlzTTL", "3600"),
            ])
        except ldap.LDAPError, e:
            self.con.delete_s(dlzhost_dn)
            raise RealmManagementError("Could not create record '%s' - LDAP add failed" % record, e)

    def create_dns_ptr_record(self, ip, name):
        _name = dns.name.from_text(str(name))
        if ip.len() > 1:
            raise RealmManagementError("The PTR IP '%s' is not a single address" % ip)
        if not _name.is_absolute():
            raise RealmManagementError("The PTR hostname '%s' is not an absolute hostname." %
                                       _name.to_text(omit_final_dot=True))
        address = dns.name.from_text(ip.reverseName())
        zone = address.parent()
        zones = self.get_reverse_zones()
        try:
            while not zone in zones:
                zone = zone.parent()
        except dns.name.NoParent:
            raise RealmManagementError("The realm is not authoritative for %s" % address)

        address = address.relativize(zone)
        record = "%s IN PTR %s to %s" % (address, _name.to_text(omit_final_dot=True), zone)
        info("Adding DNS record '%s'" % record)
        dlzhost_dn = "dlzHostName=%s,dlzZoneName=%s,cn=reverse-zones,%s,%s" % (
            address, zone.to_text(omit_final_dot=True), self.get_string_config_parameter("dns_root_rdn"), self.ldap_base
        )
        try:
            info("Creating DNS record '%s' in the '%s' realm" % (record, self.realm))
            self.con.add_s(dlzhost_dn, [("objectClass", "dlzHost")])
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not create DNS record '%s' - LDAP add failed" % record, e)
        try:
            self.con.add_s("dlzType=ptr,%s" % dlzhost_dn, [
                ("objectClass", "dlzPTRRecord"),
                ("dlzHostName", address.to_text()),
                ("dlzData", _name.to_text()),
                ("dlzRecordID", "0"),
                ("dlzTTL", "3600"),
                ])
        except ldap.LDAPError, e:
            self.con.delete_s(dlzhost_dn)
            raise RealmManagementError("Could not create record '%s' - LDAP add failed" % record, e)

    def has_dns_record(self, name):
        zones = self.get_hosted_zones_matching(["forward", "stub", "reverse"])
        _name = dns.name.from_text(str(name))
        zone = _name.parent()
        try:
            while not zone in zones:
                zone = zone.parent()
        except dns.name.NoParent:
            return False

        try:
            raw_result = self.con.search_s(
                "%s,%s" % (self.get_string_config_parameter("dns_root_rdn"), self.ldap_base),
                ldap.SCOPE_SUBTREE,
                filterstr="(&(objectClass=dlzHost)(dlzHostName=%s)(dlzZoneName:dn:=%s))" % (
                    _name.relativize(zone),
                    zone.to_text(omit_final_dot=True)
                ),
                attrlist=["dn"]
            )
            if len(raw_result) > 0:
                debug("DNS record exists for '%s'." % _name.to_text(omit_final_dot=True))
                return True
        except ldap.NO_SUCH_OBJECT:
            pass
        except ldap.LDAPError, e:
            raise RealmManagementError("", e)
        debug("DNS record for '%s' does not exist." % _name.to_text(omit_final_dot=True))
        return False

    def delete_dns_record(self, name):
        zones = self.get_hosted_zones_matching(["forward", "stub", "reverse"])
        _name = dns.name.from_text(str(name))
        zone = _name.parent()
        try:
            while not zone in zones:
                zone = zone.parent()
        except dns.name.NoParent:
            raise RealmManagementError("The realm is not authoritative for %s." % _name.to_text(omit_final_dot=True))

        try:
            raw_result = self.con.search_s(
                "%s,%s" % (self.get_string_config_parameter("dns_root_rdn"), self.ldap_base),
                ldap.SCOPE_SUBTREE,
                filterstr="(&(objectClass=dlzHost)(dlzHostName=%s)(dlzZoneName:dn:=%s))" % (
                    _name.relativize(zone),
                    zone.to_text(omit_final_dot=True)
                )
            )
            if len(raw_result) > 0:
                debug("DNS record exists for '%s'." % _name.to_text(omit_final_dot=True))
                info("Deleting DNS record '%s' in the '%s' realm." % (_name.to_text(omit_final_dot=True), self.realm))
                del_tree(self.con, ldap_search_results(raw_result)[0].get_dn())
                return
        except ldap.NO_SUCH_OBJECT:
            pass
        except ldap.LDAPError, e:
            raise RealmManagementError("Unable to delete DNS record", e)
        debug("DNS record for '%s' does not exist." % _name.to_text(omit_final_dot=True))

    def zone_relativize(self, name):
        """
        :type name: Name
        :return: tuple(Name, Name)
        """
        zones = self.get_forward_zones()
        _name = dns.name.from_text(str(name))
        zone = _name.parent()
        try:
            while not zone in zones:
                zone = zone.parent()
        except dns.name.NoParent:
            raise RealmManagementError("The realm is not authoritative for %s." % _name.to_text(omit_final_dot=True))
        relative_name = _name.relativize(zone)
        return relative_name, zone

    def has_dhcp_reservation(self, name):
        _name = dns.name.from_text(str(name))
        relative_name, zone = self.zone_relativize(_name)
        dn = "cn=%s,cn=reservations,cn=config,ou=dhcp,%s" % (relative_name, self.ldap_base)
        hostname = _name.to_text(omit_final_dot=True)
        try:
            result = ldap_search_results(
                self.con.search_s(
                    dn,
                    ldap.SCOPE_BASE,
                    filterstr="(objectClass=dhcpHost)",
                    attrlist=["dhcpStatements"]
                )
            )
            statements = result[0].get_attr_values("dhcpStatements")
            if not statements is None:
                for index, statement in enumerate(statements):
                    if re.match(r"fixed-address", statement):
                        hostnames = re.findall("(?!fixed-address) +([a-z0-9.]+)", statement)
                        if hostname in hostnames:
                            debug("DHCP reservation exists for '%s'" % hostname)
                            return True
        except ldap.NO_SUCH_OBJECT:
            pass
        except ldap.LDAPError, e:
            raise RealmManagementError("", e)
        debug("DHCP reservation does not exist for '%s'" % hostname)
        return False

    def delete_dhcp_reservation(self, name):
        _name = dns.name.from_text(str(name))
        relative_name, zone = self.zone_relativize(_name)
        dn = "cn=%s,cn=reservations,cn=config,ou=dhcp,%s" % (relative_name, self.ldap_base)
        hostname = _name.to_text(omit_final_dot=True)
        info("Deleting DHCP reservation for '%s' in the '%s' realm" % (hostname, self.realm))
        try:
            result = ldap_search_results(
                self.con.search_s(
                    dn,
                    ldap.SCOPE_BASE,
                    filterstr="(objectClass=dhcpHost)",
                    attrlist=["dhcpStatements"]
                )
            )
            statements = result[0].get_attr_values("dhcpStatements")
            if not statements is None:
                for index, statement in enumerate(statements):
                    if re.match(r"fixed-address", statement):
                        hostnames = re.findall("(?!fixed-address) +([a-z0-9.]+)", statement)
                        try:
                            hostnames.remove(hostname)
                        except ValueError:
                            break
                        statements[index] = "fixed-address %s" % ','.join(hostnames)
                        self.con.modify_s(dn, [(ldap.MOD_REPLACE, "dhcpStatements", statements)])
                        return
        except ldap.NO_SUCH_OBJECT:
            pass
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not delete DHCP reservation for '%s'" % hostname, e)
        info("There is no existing DHCP reservation for '%s'" % hostname)

    def create_dhcp_reservation(self, name):
        _name = dns.name.from_text(str(name))
        relative_name, zone = self.zone_relativize(_name)
        hostname = _name.to_text(omit_final_dot=True)
        dn = "cn=%s,cn=reservations,cn=config,ou=dhcp,%s" % (relative_name, self.ldap_base)
        info("Creating DHCP reservation for '%s' in the '%s' realm" % (hostname, self.realm))
        try:
            result = ldap_search_results(
                self.con.search_s(
                    dn,
                    ldap.SCOPE_BASE,
                    filterstr="(objectClass=dhcpHost)",
                    attrlist=["dhcpStatements"]
                )
            )
            statements = result[0].get_attr_values("dhcpStatements")
            if statements is None:
                self.con.modify_s(dn, [(ldap.MOD_ADD, "dhcpStatements", "fixed-address %s" % hostname)])
                return
            for index, statement in enumerate(statements):
                if re.match(r"fixed-address", statement):
                    hostnames = re.match(
                        "(?:fixed-address +)((?:[a-z0-9.]+)(?:,(?:[a-z0-9.]+))*)", statement
                    ).group(1).split(',')
                    if hostname in hostnames:
                        info("A DHCP reservation already exists for '%s'" % hostname)
                        return
                    hostnames.append(hostname)
                    hostnames.sort()
                    statements[index] = "fixed-address %s" % ','.join(hostnames)
                    self.con.modify_s(dn, [(ldap.MOD_REPLACE, "dhcpStatements", statements)])
                    return
            statements.append("fixed-address %s" % hostname)
            self.con.modify_s(dn, [(ldap.MOD_REPLACE, "dhcpStatements", statements)])
        except ldap.NO_SUCH_OBJECT:
            self.con.add_s(dn, [
                ("objectClass", ["dhcpHost", "dhcpOptions", "top"]),
                ("dhcpOption", 'dhcp-client-identifier "%s"' % relative_name),
                ("dhcpStatements", "fixed-address %s" % hostname),
            ])
        except ldap.LDAPError, e:
            raise RealmManagementError("Could not create DHCP reservation for '%s' - LDAP add failed" % hostname, e)

    def relativize(self, name):
        if not isinstance(name, Name):
            name = dns.name.from_text(name, origin=None)
        host, domain = name.split(len(name.labels) - 1)
        realm_zones = self.get_forward_zones()
        # for now assume the realm root domain is realm.lower()
        realm_domain = dns.name.from_text(self.realm.lower())

        # is the supplied name fully qualified?
        if not domain.is_absolute():
            if domain == dns.name.empty:
                domain = realm_domain
            else:
                domain_pattern = re.compile(glob_to_regex(domain.to_text() + '.'))
                for zone in realm_zones:
                    if domain_pattern.match(zone.to_text()):
                        domain = zone
        fqdn = host + domain
        try:
            while not domain in realm_zones:
                domain = domain.parent()
            if domain.is_subdomain(realm_domain):
                return fqdn.relativize(realm_domain), realm_domain, domain
            realm_parent = realm_domain.parent()
            if domain.is_subdomain(realm_parent):
                return fqdn.relativize(realm_parent), realm_parent, domain
            return fqdn, dns.name.empty, domain
        except dns.name.NoParent:
            raise RealmManagementError("The realm is not authoritative for %s" % fqdn.to_text(omit_final_dot=True))

    def extract_keytab(self, principal, keytab):
        try:
            self.kadmin.ktadd(principal, keytab)
        except Exception, e:
            raise RealmManagementError("Could not extract keytab for '%s' - ktadd failed" % principal, e)

    def fully_qualified_domain_name(self, name=None):
        #todo
        return name

    def fully_qualified_host_name(self, name=None):
        if not name:
            return socket.getfqdn()
        dot_matcher = re.compile('\.')
        hostname, domain = dot_matcher.split(name, 1)
        return "%s.%s" % (hostname, self.fully_qualified_domain_name(domain))


        # determine the realm-relative hostname to add to the LDAP store
        # site domains are relative to root_domain rather than realm_domain
        # so strip the longer of the two to obtain the realm-relative hostname
        # below are examples for the realm TEST.WOTIFGROUP.COM:
        #
        # <execution host>        <supplied hostname>    <site domain>         <realm-relative hostname>
        # bar.test.wotifgroup.com   foo                     test.wotifgroup.com  foo
        # bar.test.wotifgroup.com   foo.test                test.wotifgroup.com  foo
        # bar.test.wotifgroup.com   foo.test.wotifgroup.com test.wotifgroup.com  foo
        # bar.test.wotifgroup.com   foo.tst1                tst1.wotifgroup.com  foo.tst1
        # bar.test.wotifgroup.com   foo.tst1.wotifgroup.com tst1.wotifgroup.com  foo.tst1
        # zip.tst2.wotifgroup.com   baz                     tst2.wotifgroup.com  baz.tst2
        # zip.tst2.wotifgroup.com   baz.test                test.wotifgroup.com  baz
        # zip.tst2.wotifgroup.com   baz.test.wotifgroup.com test.wotifgroup.com  baz
        # zip.tst2.wotifgroup.com   baz.tst2                tst2.wotifgroup.com  baz.tst2
        # zip.tst2.wotifgroup.com   baz.tst2.wotifgroup.com tst2.wotifgroup.com  baz.tst2
