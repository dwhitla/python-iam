import ldap
import ldif
from StringIO import StringIO
from ldap.cidict import cidict

__all__ = ['ldap_search_results', 'LDAPSearchResult']


def ldap_search_results(raw_result):
    """Given a set of results, return a list of LDAPSearchResult objects."""
    result = []
    if len(raw_result) > 0:
        arr = None
        if type(raw_result) == tuple and len(raw_result) == 2:
            (code, arr) = raw_result
        elif type(raw_result) == list:
            arr = raw_result

        for item in arr:
            result.append(LDAPSearchResult(item))

    return result


#LDAPAttribute = collections.namedtuple("LDAPAttribute", "name values")

class LDAPSearchResult(object):
    """A class to model LDAP results."""
    dn = ''

    def __init__(self, entry_tuple):
        """Create a new LDAPSearchResult object."""
        (dn, attrs) = entry_tuple
        if not dn:
            raise ValueError("DN is mandatory")
        self.dn = dn
        self.set_attributes(attrs)

    def get_attributes(self):
        """
        Get a dictionary of all attributes.
        get_attributes()->{'name1':['value1','value2',...], 'name2: [value1...]}
        """
        return self.attrs

    def set_attributes(self, attr_dict):
        """
        Set the list of attributes for this record.
        The format of the dictionary should be string key, list of string values.
        e.g. {'cn': ['M Butcher','Matt Butcher']}
        set_attributes(attr_dictionary)
        """
        self.attrs = cidict(attr_dict)

    def has_attribute(self, attr_name):
        """
        Returns true if there is an attribute by this name in the record.
        has_attribute(string attr_name)->boolean
        """
        return attr_name in self.attrs

    def get_attr_values(self, attr_name):
        """
        Get a list of attribute values.
        get_attr_values(string key)->['value1','value2']
        """
        return self.attrs.get(attr_name, None)

    def get_attr_names(self):
        """
        Get a list of attribute names.
        get_attr_names()->['name1','name2',...]
        """
        return self.attrs.keys()

    def get_dn(self):
        """
        Get the DN string for the record.
        get_dn()->string dn
        """
        return self.dn

    def pretty_print(self):
        """
        Create a nice string representation of this object.
        pretty_print()->string
        """
        str_ = "DN: " + self.dn + "\n"
        for a, v_list in self.attrs.iteritems():
            str_ = str_ + "Name: " + a + "\n"
            for v in v_list:
                str_ = str_ + " Value: " + v + "\n"
                str_ += "========"
        return str_

    def to_ldif(self):
        """
        Get an LDIF representation of this record.
        to_ldif()->string
        """
        out = StringIO()
        ldif_out = ldif.LDIFWriter(out)
        ldif_out.unparse(self.dn, self.attrs)
        return out.getvalue()


def del_tree(con, dn):
    nodes = [dn]
    while nodes:
        node = nodes[0]
        try:
            con.delete_s(node)
            nodes.pop(0)
        except ldap.NOT_ALLOWED_ON_NONLEAF:
            # add all the leaves to nodes
            leaves = con.search_s(node, ldap.SCOPE_ONELEVEL)
            for leaf in ldap_search_results(leaves):
                nodes.insert(0, leaf.get_dn())
